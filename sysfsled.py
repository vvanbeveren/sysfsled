#
# Library to bind to sysfsled
#
from multiprocessing.sharedctypes import Value
from pathlib import Path
import errno
from typing import List, Any, Tuple, Set

SYSFS_LED_PATH=Path("/sys/class/leds")

T_NONE = "none"
T_HEARTBEAT = "heartbeat"
T_ONESHOT = "oneshot"
T_TIMER =  "timer"
T_MDT = "mdt"
T_CPU = "cpu"
T_ONESHOT = "oneshot"
T_ACTIVITY ="activity"
T_TRANSIENT ="transient"
T_FLASH ="flash"
T_TORCH ="torch"
T_DEFAULT_ON = "default-on"
T_MMC ="mmc"
T_GPIO ="gpio"

A_TRIGGER = "trigger"
A_MAX_BRIGHTNESS = "max_brightness"
A_BRIGHTNESS = "brightness"
A_INVERT = "invert"
A_DELAY_ON = "delay_on"
A_DELAY_OFF = "delay_off"
A_SHOT = "shot"
A_DURATION = "duration"
A_STATE = "state"
A_ACTIVATE = "activate"
A_DESIRED_BRIGHTNESS = "desired_brightness"
A_GPIO = "gpio"

class LedIface:
    """
        Low level LED interface. Generally you do not instantiate this class manualy, but 
        use <led instance>.iface, which would return this interface.

        This interface provides access to the attributes of the LED as if it was item access, for example

            myled.iface['trigger'] = "heartbeat"

        It can also be used to read the attribute.

            print(myled.iface['invert'])
    """
    
    def __init__(self, path : Path):
        """Initialize a new Led interface using provided path"""

        if not path.is_dir():
            raise RuntimeError("Invalid LED {}".format(path.name))
        self._path = path
        
    def _get_file(self, name: str) -> Path:
        f = self._path / name
        if not f.is_file():
            raise RuntimeError("Invalid LED property {}".format(name))
        return f

    def __contains__(self, name : str) -> bool:
        """
        Returns whether or not the attribute exists.
        """
        return ( self._path / name ).is_file()

    def __setitem__(self, name: str, value: str or int) -> None:
        if not isinstance(value, (int, str)):
            raise ValueError("Value must be either int or str")

        value = str(value)

        try:
            self._get_file(name).write_text(value, encoding='ascii')
        except OSError as oe:
            if oe.errno == errno.EINVAL:
                raise ValueError("Can not set {} to {}".format(name, value)) from None
            else:
                raise
    
    def __getitem__(self, name: str) -> str:
        return self._get_file(name).read_text(encoding='ascii').strip()


class Led:
    """
    high-level LED interface. 

    Most common LED interface options are accessible from this class.

    If low-level access is required, the low-level interface can be obtained
    using the iface property.
    """

    def __init__(self, name : str, basepath : Path = SYSFS_LED_PATH):
        """
        Initiates a Led. Generally you don't need to create Leds directly

        You can use the module level get_led() and get_leds() functions.
        """
        self._iface = LedIface(basepath / name)

        self._trigs = set()
        for trigger in self._iface[A_TRIGGER].split():
            if trigger.startswith("[") and trigger.endswith("]"):
                trigger = trigger[1:-1]
            self._trigs.add(trigger)

    def is_trigger_supported(self, trigger: str) -> bool:
        _, trigs = self.get_triggers()
        return trigger in trigs

    def get_triggers(self) -> Set[str]:
        """
        Returns a list of supported triggers.        
        """
        return self._trigs.copy()

    def get_trigger(self) -> str:
        """
        Get the current trigger
        """

        trigs = self.iface[A_TRIGGER]
        start = trigs.find('[')
        end = trigs.find(']', start)
        if start == -1 or end == -1:
            raise RuntimeError("Invalid trigger return value")
        return trigs[start+1:end]
        
    def set_trigger(self, trigger : str) -> None:
        """
        Sets the trigger. For most common triggers, you can also used the compound functions
        such as heartbeat(), and timer().
        """
        self._iface[A_TRIGGER] = trigger

    @property
    def iface(self):
        """Return the direct interface to the LED"""
        return self._iface

    def set_brightness(self, brightness : float) -> int:
        """
        Set LED brightness. Expected is a value between 0 (off) and 1 (max brightness).

        Returns the actual integer value applied.
        """
        if A_MAX_BRIGHTNESS not in self.iface:
            raise RuntimeError("Not supported (for this trigger)")

        new_value = int(brightness * int(self.iface[A_MAX_BRIGHTNESS]) + 0.5)
        self.iface[A_BRIGHTNESS] = new_value

    def get_brightness(self) -> float:
        if A_MAX_BRIGHTNESS not in self.iface:
            raise RuntimeError("Not supported (for this trigger)")
        
        return float(self.iface[A_BRIGHTNESS]) / float(self.iface[A_MAX_BRIGHTNESS])

    def trigger_timer(self, on_delay : int, off_delay : int) -> None:
        """
        Set the trigger to timer, and configure the delay.

        Delay is in milliseconds.
        """
        if T_TIMER not in self._trigs:
            raise RuntimeError("Timer trigger not supported")

        self.iface[A_TRIGGER] = T_TIMER
        self.iface[A_DELAY_ON]= on_delay
        self.iface[A_DELAY_OFF] = off_delay
        
    def trigger_heartbeat(self, invert : bool = False) -> None:
        if T_HEARTBEAT not in self._trigs:
            raise RuntimeError("Heartbeat trigger not supported")

        self.iface[A_TRIGGER] = T_HEARTBEAT
        self.iface[A_INVERT] = 1 if invert else 0

    def trigger_cpu(self, number=None) -> None:
        """
        LED state shows CPU state.
        
        Leave the number for just general load. For a specific CPU, select a number.
        """
        if number is None:
            cpu = T_CPU
        else:
            cpu = "{}{}".format(T_CPU, number)

        if cpu not in self._trigs:
            raise RuntimeError("{} trigger not supported".format(cpu))

        self.iface[A_TRIGGER] = cpu
    
    def trigger_none(self) -> None:
        """
        No trigger
        """
        if T_NONE not in self._trigs:       # Unlikely....
            raise RuntimeError("None trigger not supported")

        self.iface[A_TRIGGER] = T_NONE

    def trigger_oneshot(self,  on_delay: int, off_delay: int,  invert: bool = False) -> None:
        """
        One shot triggering. Use shot() function to feed the LED
        """
        if T_ONESHOT not in self._trigs:
            raise RuntimeError("Oneshot trigger not supported")

        self.iface[A_TRIGGER] = T_ONESHOT
        self.iface[A_DELAY_ON]= on_delay
        self.iface[A_DELAY_OFF] = off_delay
        self.iface[A_INVERT] = 1 if invert else 0

    def do_shot(self) -> None:
        """
        Execute a single shot
        """
        if A_SHOT not in self.iface:
            raise RuntimeError("Not in oneshot mode")
        
        self.iface[A_SHOT] = 1

    def trigger_transient(self, duration: int, state: bool = True) -> None:
        """
        Transient triggering. Use do_activate() to activate.
        """
        if T_TRANSIENT not in self._trigs:
            raise RuntimeError("Transient trigger not supported")

        self.iface[A_TRIGGER] = T_ONESHOT
        self.iface[A_DURATION]= duration
        self.iface[A_STATE] = 1 if state else 0

    def do_activate(self) -> None:
        """
        Activate a transient
        """
        if A_ACTIVATE not in self.iface:
            raise RuntimeError("Not in transient mode")
        
        self.iface[A_ACTIVATE] = 1

    def trigger_flash(self) -> None:
        """
        Flash triggered. 
        
        TODO: Should be extended, if we understand its purpose
        """
        if T_FLASH not in self._trigs:
            raise RuntimeError("Flash trigger not supported")

        self.iface[A_TRIGGER] = T_FLASH

    def trigger_torch(self) -> None:
        """
        Torch triggered. 
        
        TODO: Should be extended if we understand its purpose
        """
        if T_TORCH not in self._trigs:
            raise RuntimeError("Torch trigger not supported")

        self.iface[A_TRIGGER] = T_TORCH

    def trigger_mmc(self, number : int) -> None:
        """
        MMC triggered
        """
        t_mmc = "{}{}".format(T_MMC, number)
        if t_mmc not in self._trigs:
            raise RuntimeError("{} trigger not supported".format(t_mmc))

        self.iface[A_TRIGGER] = t_mmc

    def trigger_gpio(self, gpio_no : int, desired_brightness : float = None, invert : bool = False) -> None:
        """
        GPIO triggered

        :param gpio_no: GPIO number
        :param desired_brightness: Desired brightness when triggered (0..1] (note that 0 is not supported, and would result in full brightnesss)
        :param invert: Invert GPIO before processing.

        Not sure why this trigger uses its own brightness setting....
        """
        if T_GPIO not in self._trigs:
            raise RuntimeError("GPIO trigger not supported")

        self.iface[A_TRIGGER] = T_GPIO
        self.iface[A_GPIO] = gpio_no
        self.iface[A_INVERT] = 1 if invert else 0

        if desired_brightness is None:
            self.iface[A_DESIRED_BRIGHTNESS] = 0
        else:
            self.iface[A_DESIRED_BRIGHTNESS] = int(desired_brightness * int(self.iface[A_MAX_BRIGHTNESS]) + 0.5)

    def trigger_default_on(self):
        if T_DEFAULT_ON not in self._trigs:
            raise RuntimeError("Default-on trigger not supported")

        self.iface[A_TRIGGER] = T_DEFAULT_ON


def get_leds(basepath : Path = SYSFS_LED_PATH):
    """
    Returns a map of all available LEDs, as Led instances.
    """
    return {
        children.name: Led(children.name, basepath) 
            for children in basepath.iterdir()
    }

def get_led(name : str, basepath : Path = SYSFS_LED_PATH):
    """
    Get a specific LED.
    """
    return Led(name, basepath)
    

if __name__ == "__main__":

    hb = get_led("heartbeat")
    print(hb.get_triggers())
    print(hb.get_trigger())
    hb.set_brightness(0.5)
    hb.trigger_timer(500, 500)

    
